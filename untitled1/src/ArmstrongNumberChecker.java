import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class ArmstrongNumberChecker {

    @Mock
    FileInput file;
    ArmstrongNumber a;
    ArmstrongNumber a1;

    @Spy
    FileInput file1;

    @Before
    public void setUp() throws Exception{
        file= mock(FileInput.class);
        a=new ArmstrongNumber();
        a.setReadObject(file);

        a1=new ArmstrongNumber();
        file1=spy(new FileInput());
        a1.setReadObject(file);

        System.out.println("This is executed before ");
    }

    @Test
    public void fileAccess() throws Exception{
        file= mock(FileInput.class);
        a=new ArmstrongNumber();
        a.setReadObject(file);
        when(file.FileInput(anyString())).thenReturn(10);
        boolean answer= a.checkArmstrongNumber();
        verify(file).FileInput(anyString());
    }

    @Test
    public void checkArmstrongNumber() throws Exception{
        file= mock(FileInput.class);
        a=new ArmstrongNumber();
        a.setReadObject(file);
        when(file.FileInput(anyString())).thenReturn(153);
        assertTrue(a.checkArmstrongNumber());
    }

    @Test
    public void invalidArmstrong() throws Exception{
        a1=new ArmstrongNumber();
        file1=spy(new FileInput());
        a1.setReadObject(file1);
        doReturn(10).when(file1).FileInput(anyString());
        assertFalse(a.checkArmstrongNumber());
    }
    
    //Exception Case
    
    @Test
    public void isNonInteger() throws Exception{   
        try {
            FileInput fi=new FileInput();
            int number = fi.FileInput("test.txt");
            System.out.println(number);
        }
        catch (Exception e) {
           System.out.println("Please provide an integer" +e);
        }
    }
    
}
